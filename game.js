
var imageSize = 100;
var hitRadius = imageSize / 3;

var gameInProgress = true;

var gameOptions = {
  height: '100%',
  width: '100%',
  nEnemies: 20,
  padding:0
};

var gameStats = {
  score: 0 ,
  bestScore: 0
};

var scoreInterval;
var moveInterval;

var gameBoard = d3.select('.container').append('svg:svg').attr("id", "gameBoard")
                .attr('width', gameOptions.width)
                .attr('height', gameOptions.height).style("top","0%").style("left","0%");

var gameBoardWidth = document.getElementById('gameBoard').clientWidth
var gameBoardHeight = document.getElementById('gameBoard').clientHeight


var axes = {
  x: d3.scale.linear().domain([0,100]).range([0,gameBoardWidth]),
  y: d3.scale.linear().domain([0,100]).range([0,gameBoardHeight])
};

var updateScore = function(){
  d3.select('#current-score').text(gameStats.score.toString());
};

var updateBestScore = function(){
  gameStats.bestScore = _.max([gameStats.bestScore, gameStats.score]);
  d3.select('#best-score').text(gameStats.bestScore.toString());
};

var Player = function(gameOptions){
  this.fill = '#ff6600';
  this.x = 0;
  this.y = 0;
  this.angle = 0;
  this.r = hitRadius;
  this.gameOptions = gameOptions;
}

Player.prototype.render = function(to){

   this.el = to.append('svg:image')
           .attr('width', imageSize)
   .attr('height', imageSize)
   .attr("xlink:href","media/cidhighwind.png").attr("transform","translate(" + imageSize/-2 +","+ imageSize/-2 +")")

   this.transform({
     x: gameBoardWidth * 0.5,
     y: gameBoardHeight * 0.5
   })

   this.setupDragging()
   return this;
}

Player.prototype.getX = function() {
      return this.x;
    };

    Player.prototype.setX = function(x) {
      var maxX, minX;
      minX = this.gameOptions.padding;
      maxX = this.gameOptions.width - this.gameOptions.padding;
      if (x <= minX) x = minX;
      if (x >= maxX) x = maxX;
      return this.x = x;
    };

    Player.prototype.getY = function() {
      return this.y;
    };

    Player.prototype.setY = function(y) {
      var maxY, minY;
      minY = this.gameOptions.padding;
      maxY = gameBoardHeight - this.gameOptions.padding;
      if (y <= minY) y = minY;
      if (y >= maxY) y = maxY;
      return this.y = y;
    };

Player.prototype.transform = function (opts) {
  this.angle = opts.angle || this.angle;

  this.setX(opts.x || this.x);
  this.setY(opts.y || this.y);

  return this.el.attr('transform', ("rotate(" + this.angle + "," + (this.getX()) + "," + (this.getY()) + ") ") + ("translate(" + (this.getX()+ imageSize/-2) + "," + (this.getY()+ imageSize/-2) + ")"));
}

Player.prototype.moveAbsolute = function (x,y) {
  return this.transform({x:x,y:y})
}

Player.prototype.moveRelative = function(dx, dy) {
  return this.transform({
    x: this.getX() + dx,
    y: this.getY() + dy,
    angle: 0
  });
};

Player.prototype.setupDragging = function() {
  var drag, dragMove,
    _this = this;
  dragMove = function() {
    return _this.moveRelative(d3.event.dx, d3.event.dy);
  };
  drag = d3.behavior.drag().on('drag', dragMove);
  return this.el.call(drag);
};


var players = [];

var enemyImageArray = ["media/shiva.png","media/phoenix.png", "media/leviathan.png", "media/ifrit.png", "media/golbez.png",, "media/bahamut.png"]

players.push(new Player(gameOptions).render(gameBoard));

var createEnemies = function() {
    return _.range(0, gameOptions.nEnemies).map(function(i) {
      return {
        id: i,
        x: Math.random() * 100,
        y: Math.random() * 100,
        imagePath: enemyImageArray[i%enemyImageArray.length]
      };
    });
  };

var render = function(enemy_data) {
    var enemies = gameBoard.selectAll('image.enemy').data(enemy_data, function(d) {
      return d.id;
    });
    enemies.enter().append('svg:image').attr('class', 'enemy').attr('x', function(enemy) {
      return axes.x(enemy.x);
    }).attr('y', function(enemy) {
      return axes.y(enemy.y);
    }).attr('r', 0).attr('width', imageSize)
   .attr('height', imageSize).attr("transform","translate(" + imageSize/-2 +","+ imageSize/-2 +")")
   .attr("xlink:href", function(enemy){return enemy.imagePath})
    enemies.exit().remove();
    var checkCollision = function(enemy, collidedCallback) {
      return _(players).each(function(player) {
        var radiusSum = parseFloat(enemy.attr('r')) + player.r;
        var xDiff = parseFloat(enemy.attr('x')) - player.x;
        var yDiff = parseFloat(enemy.attr('y')) - player.y;
        var separation = Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
        if (separation < radiusSum) return collidedCallback(player, enemy);
      });
    };
    var onCollision = function() {
       if (gameInProgress) {
         updateBestScore();

        gameStats.score = 0;

        updateScore();


        stop();
      }

    };

    var tweenWithCollisionDetection = function(endData) {

      var enemy = d3.select(this);
      var startPos = {
        x: parseFloat(enemy.attr('x')),
        y: parseFloat(enemy.attr('y'))
      };
      var endPos = {
        x: axes.x(endData.x),
        y: axes.y(endData.y)
      };
      return function(t) {

        checkCollision(enemy, onCollision);
        var enemyNextPos = {
          x: startPos.x + (endPos.x - startPos.x) * t,
          y: startPos.y + (endPos.y - startPos.y) * t
        };
        return enemy.attr('x', enemyNextPos.x).attr('y', enemyNextPos.y);
      };
    };
    return enemies.transition().duration(500).attr('r', hitRadius).transition().duration(2000).tween('custom', tweenWithCollisionDetection);
  };

var  play = function() {

    gameInProgress = true;

    var gameTurn = function() {
      var newEnemyPositions;
      newEnemyPositions = createEnemies();
      return render(newEnemyPositions);
    };

    var increaseScore = function() {
      gameStats.score += 1;
      return updateScore();
    };

    gameTurn();


    moveInterval = setInterval(gameTurn, 2000);

    scoreInterval = setInterval(increaseScore, 50);

  };

  var  stop = function() {
    clearInterval(moveInterval);
    clearInterval(scoreInterval);

    var enemies = gameBoard.selectAll('image.enemy').data([]).exit().transition().duration(800).style('opacity', 0)
    players[0].el.data([]).exit().transition().duration(800).style('opacity', 0)

    var killKennyWidth = 300;
    gameBoard.append('svg:image')
                .attr('width', killKennyWidth)
                .attr('height', killKennyWidth).attr('x', '50%').attr('y', '50%').attr("transform","translate(" + killKennyWidth/-2 +","+ killKennyWidth/-2 +")").attr('id', "killKenny").attr("xlink:href","media/ghost.jpg")


    gameInProgress = false;

    gameBoard.on("mousedown",function() {
      gameBoard.on("mousedown",null);
      reset();
    })

  }

  var reset = function() {
    //if (!gameInProgress) {
    gameBoard.selectAll('#killKenny').data([]).exit().transition().duration(800).style('opacity', 0).remove()

    players[0] = new Player(gameOptions).render(gameBoard);

    play();

    var enemies = gameBoard.selectAll('image.enemy').transition().duration(800).style('opacity', 1)
 // }
  }


  play();
